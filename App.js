import React from 'react';
// import {Text, View} from 'react-native';
import MainNavigator from './MainNavigator';
import {NavigationContainer} from '@react-navigation/native';
// import HomeScreen from './src/components/home/HomeScreen';

const App = () => {
  return (
    // <HomeScreen />
    <NavigationContainer>
      <MainNavigator />
    </NavigationContainer>
  );
};
export default App;
