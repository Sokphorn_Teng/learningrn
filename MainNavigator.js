import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import StartScreen from './src/components/StartScreen';
import EnviromentScreen from './src/components/EnviromentScreen';
import HomeScreen from './src/components/home/HomeScreen';
import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';

import IntroductScreen from './src/components/toptap/IntroductScreen';
import ComponentScreen from './src/components/toptap/ComponentScreen';
import NotedScreen from './src/components/toptap/NotedScreen';
import ReactScreen1 from './src/components/react/ReactScreen1';
import ComponentScreens from './src/components/ComponentScreens';
import {Image} from 'react-native';

const Stack = createStackNavigator();
const Top = createMaterialTopTabNavigator();

const MenuBar = () => {
  return (
    <>
      <Image source={require('./src/assets/images/menu.png')} />
    </>
  );
};

const TopScreen = () => {
  return (
    <Top.Navigator>
      <Top.Screen name="introduction" component={IntroductScreen} />
      <Top.Screen name="component" component={ComponentScreen} />
      <Top.Screen name="note" component={NotedScreen} />
    </Top.Navigator>
  );
};

const MainNavigator = () => {
  return (
    <Stack.Navigator>
      {/* Start screen */}
      <Stack.Screen
        name="Home"
        component={HomeScreen}
        options={{
          header: (props) => <MenuBar {...props} />,
        }}
      />
      <Stack.Screen name="starting" component={TopScreen} />
      <Stack.Screen name="setup" component={EnviromentScreen} />
      <Stack.Screen name="component" component={ComponentScreens} />

      {/* element screen */}
      <Stack.Screen name="React-Native" component={ReactScreen1} />
    </Stack.Navigator>
  );
};

export default MainNavigator;
