import React from 'react';
import {Text, View, ScrollView, Button} from 'react-native';

const ComponentScreens = () => {
  return (
    <ScrollView style={{flex: 1}}>
      <Text
        style={{
          marginTop: 30,
          textAlign: 'center',
          fontSize: 20,
          fontWeight: 'bold',
          color: '#00b09b',
        }}>
        Components of React Native
      </Text>
      <View style={{width: 430, marginTop: 25}}>
        <Text style={{marginHorizontal: 40, fontSize: 18}}>
          <Text style={{color: '#00b09b', fontWeight: 'bold'}}>Button: </Text>
          This button is not customize color or width, hight it always stand
          defualt
        </Text>
        <Text style={{marginHorizontal: 40, fontSize: 18, marginTop: 20}}>
          <Text style={{color: '#00b09b', fontWeight: 'bold'}}>Flatlist: </Text>{' '}
          is the way for take data in arrive if we have many data in project
        </Text>
        <Text style={{marginHorizontal: 40, fontSize: 18, marginTop: 20}}>
          <Text style={{color: '#00b09b', fontWeight: 'bold'}}>Image: </Text> it
          take image and it have two take of image require for image in local
          and url for image from broswer
        </Text>
        <Text style={{marginHorizontal: 40, fontSize: 18, marginTop: 20}}>
          <Text style={{color: '#00b09b', fontWeight: 'bold'}}>Modal:</Text> is
          modal that we want to show something that it alert of Modal
        </Text>
        <Text style={{marginHorizontal: 40, fontSize: 18, marginTop: 20}}>
          <Text style={{color: '#00b09b', fontWeight: 'bold'}}>TextInput:</Text>{' '}
          it for input the text if we want to put like user or password
        </Text>
        <Text style={{marginHorizontal: 40, fontSize: 18, marginTop: 20}}>
          <Text style={{color: '#00b09b', fontWeight: 'bold'}}>
            TouchableOpacity:
          </Text>{' '}
          it's a button that we can customize it and it has three more type
          TouchableHighlight and TouchableWithoutFeedback
        </Text>
        <Text style={{marginHorizontal: 40, fontSize: 18, marginTop: 20}}>
          <Text style={{color: '#00b09b', fontWeight: 'bold'}}>
            View and Text:
          </Text>{' '}
          is the componet that View is for contain text and Text for write text
        </Text>
        <Text style={{marginHorizontal: 40, fontSize: 18, marginTop: 20}}>
          <Text style={{color: '#00b09b', fontWeight: 'bold'}}>
            ScrollView and SafeAreaView:
          </Text>{' '}
          are the component that ScrollView for screen that contain more text or
          something else for scrool on the screen and SafeAreaView is for ios
          that have on screen
        </Text>
        <Text style={{marginHorizontal: 40, fontSize: 18, marginTop: 20}}>
          <Text style={{color: '#00b09b', fontWeight: 'bold'}}>
            ActivityIndicator:
          </Text>{' '}
          it's a component that we can put it for waiting like request
        </Text>
        <Text style={{marginHorizontal: 40, fontSize: 18, marginTop: 20}}>
          <Text style={{color: '#00b09b', fontWeight: 'bold'}}>Switch: </Text>{' '}
          it's a component that we can switch for notefication or something else
        </Text>
      </View>
    </ScrollView>
  );
};
export default ComponentScreens;
