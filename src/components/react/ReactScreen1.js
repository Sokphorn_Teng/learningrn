import React from 'react';
import {Text, View} from 'react-native';

const ReactScreen1 = () => {
  return (
    <View style={{flex: 1, alignItems: 'center'}}>
      <Text
        style={{
          marginTop: 30,
          fontSize: 25,
          color: '#00b09b',
          fontWeight: 'bold',
        }}>
        React Native
      </Text>
      <View style={{width: 430, marginTop: 25}}>
        <Text style={{marginHorizontal: 40, fontSize: 18}}>
          <Text style={{color: '#00b09b', fontWeight: 'bold'}}>
            React Native:
          </Text>{' '}
          is an open source mobile application framework that created by
          Facebook.
        </Text>
        <Text style={{marginHorizontal: 40, fontSize: 18, marginTop: 20}}>
          <Text style={{color: '#00b09b', fontWeight: 'bold'}}>
            React Native:
          </Text>{' '}
          is a great tool to develop hybrid app (IOS & Android).
        </Text>
        <Text style={{marginHorizontal: 40, fontSize: 18, marginTop: 20}}>
          <Text style={{color: '#00b09b', fontWeight: 'bold'}}>
            React Native:
          </Text>{' '}
          had written in JavaScript, Java, C++, Objective-C, Python
        </Text>
        <Text style={{marginHorizontal: 40, fontSize: 18, marginTop: 20}}>
          <Text style={{color: '#00b09b', fontWeight: 'bold'}}>
            React Native:
          </Text>{' '}
          need you to have an understanding of JavaScript fundamentals
        </Text>
        <Text style={{marginHorizontal: 40, fontSize: 18, marginTop: 20}}>
          <Text style={{color: '#00b09b', fontWeight: 'bold'}}>
            React Native:
          </Text>{' '}
          has concepts behind of React are Component, JSX, Props and State
        </Text>
      </View>
    </View>
  );
};
export default ReactScreen1;
