import {Link} from '@react-navigation/native';
import React from 'react';
import {Text, View, ScrollView} from 'react-native';

const EnviromentScreen = () => {
  return (
    <ScrollView style={{flex: 1}}>
      <Text
        style={{
          marginTop: 40,
          color: '#00b09b',
          fontWeight: 'bold',
          fontSize: 25,
          textAlign: 'center',
        }}>
        Enviroment Setup
      </Text>
      <View style={{width: 420, marginTop: 25}}>
        <Text style={{marginHorizontal: 20, fontSize: 18}}>
          <Text style={{color: '#00b09b', fontWeight: 'bold'}}>Project:</Text>{' '}
          (CLI, Window, Android) need to install (choco install -y
          nodejs.install openjdk8) and install Android Studio after that config
          with Android SDK, Android SDK Platform and Android Virtual Device and
          create project (npx react-native init projectName) don't forget to
          install yarn or npm and this is comment to run project (npx
          react-native run-android).
        </Text>
      </View>
      <View style={{width: 420, marginTop: 25}}>
        <Text style={{marginHorizontal: 20, fontSize: 18}}>
          <Text style={{color: '#00b09b', fontWeight: 'bold'}}>Modal:</Text>{' '}
        </Text>
      </View>
      <View style={{width: 420, marginTop: 25}}>
        <Text style={{marginHorizontal: 20, fontSize: 18}}>
          <Text style={{color: '#00b09b', fontWeight: 'bold'}}>
            Navigation:
          </Text>{' '}
          Before starting with Navigation we need to install native (yarn add
          @react-navigation/native) and make sure your computer use yarn package
          and then install dependencies (yarn add react-native-reanimated
          react-native-gesture-handler react-native-screens
          react-native-safe-area-context @react-native-community/masked-view)
          after install stack (yarn add @react-navigation/stack) and drawer
          (yarn add @react-navigation/drawer) and top tap (yarn add
          @react-navigation/material-top-tabs react-native-tab-view) and (yarn
          add @react-navigation/bottom-tabs) after that don't forget to import
          it (import createStackNavigator from '@react-navigation/stack';)
          (import createNativeStackNavigator from
          'react-native-screens/native-stack';) (import createDrawerNavigator
          from '@react-navigation/drawer';) (import createBottomTabNavigator
          from '@react-navigation/bottom-tabs';) (import
          createMaterialTopTabNavigator from
          '@react-navigation/material-top-tabs';)
        </Text>
      </View>
    </ScrollView>
  );
};
export default EnviromentScreen;
