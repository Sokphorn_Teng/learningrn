import React from 'react';
import {Text, View} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';

const NotedScreen = () => {
  return (
    <View style={{flex: 1}}>
      <TouchableOpacity
        style={{
          width: 370,
          height: 450,
          borderWidth: 1,
          marginTop: 50,
          marginHorizontal: 20,
          borderRadius: 20,
        }}>
        <Text style={{textAlign: 'center', marginTop: 10, fontSize: 20}}>
          Noted
        </Text>
        <View style={{marginTop: 15}}>
          <Text style={{marginLeft: 40, fontSize: 40}}>
            -{' '}
            <Text style={{fontSize: 15}}>
              Class Component and Function Componet
            </Text>
          </Text>
          <Text style={{marginLeft: 40, fontSize: 40}}>-</Text>
          <Text style={{marginLeft: 40, fontSize: 40}}>-</Text>
          <Text style={{marginLeft: 40, fontSize: 40}}>-</Text>
          <Text style={{marginLeft: 40, fontSize: 40}}>-</Text>
          <Text style={{marginLeft: 40, fontSize: 40}}>-</Text>
        </View>
      </TouchableOpacity>
    </View>
  );
};
export default NotedScreen;
