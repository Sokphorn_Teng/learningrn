import React from 'react';
import {Text, View, Image, TouchableOpacity} from 'react-native';

const IntroductScreen = ({navigation}) => {
  return (
    <View style={{flex: 1}}>
      <TouchableOpacity
        onPress={() => navigation.navigate('React-Native')}
        style={{
          width: 350,
          height: 50,
          backgroundColor: 'white',
          borderWidth: 1,
          borderColor: 'grey',
          borderRadius: 15,
          marginTop: 25,
          marginHorizontal: 25,
        }}>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'center',
            marginLeft: 20,
          }}>
          <Text
            style={{
              fontSize: 20,
              fontWeight: 'bold',
              // textAlign: 'center',
              marginTop: 10,
            }}>
            React Native
          </Text>
          <Image
            style={{marginTop: 18, marginLeft: 150}}
            source={require('../../assets/images/Arrow.png')}
          />
        </View>
      </TouchableOpacity>
      <TouchableOpacity
        style={{
          width: 350,
          height: 50,
          backgroundColor: 'white',
          borderWidth: 1,
          borderColor: 'grey',
          borderRadius: 15,
          marginTop: 25,
          marginHorizontal: 25,
        }}>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'center',
            marginLeft: 20,
          }}>
          <Text
            style={{
              fontSize: 20,
              fontWeight: 'bold',
              // textAlign: 'center',
              marginTop: 10,
            }}>
            React Native
          </Text>
          <Image
            style={{marginTop: 18, marginLeft: 150}}
            source={require('../../assets/images/Arrow.png')}
          />
        </View>
      </TouchableOpacity>
      <TouchableOpacity
        style={{
          width: 350,
          height: 50,
          backgroundColor: 'white',
          borderWidth: 1,
          borderColor: 'grey',
          borderRadius: 15,
          marginTop: 25,
          marginHorizontal: 25,
        }}>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'center',
            marginLeft: 20,
          }}>
          <Text
            style={{
              fontSize: 20,
              fontWeight: 'bold',
              // textAlign: 'center',
              marginTop: 10,
            }}>
            React Native
          </Text>
          <Image
            style={{marginTop: 18, marginLeft: 150}}
            source={require('../../assets/images/Arrow.png')}
          />
        </View>
      </TouchableOpacity>
      <TouchableOpacity
        style={{
          width: 350,
          height: 50,
          backgroundColor: 'white',
          borderWidth: 1,
          borderColor: 'grey',
          borderRadius: 15,
          marginTop: 25,
          marginHorizontal: 25,
        }}>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'center',
            marginLeft: 20,
          }}>
          <Text
            style={{
              fontSize: 20,
              fontWeight: 'bold',
              // textAlign: 'center',
              marginTop: 10,
            }}>
            React Native
          </Text>
          <Image
            style={{marginTop: 18, marginLeft: 150}}
            source={require('../../assets/images/Arrow.png')}
          />
        </View>
      </TouchableOpacity>
      <TouchableOpacity
        style={{
          width: 350,
          height: 50,
          backgroundColor: 'white',
          borderWidth: 1,
          borderColor: 'grey',
          borderRadius: 15,
          marginTop: 25,
          marginHorizontal: 25,
        }}>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'center',
            marginLeft: 20,
          }}>
          <Text
            style={{
              fontSize: 20,
              fontWeight: 'bold',
              // textAlign: 'center',
              marginTop: 10,
            }}>
            React Native
          </Text>
          <Image
            style={{marginTop: 18, marginLeft: 150}}
            source={require('../../assets/images/Arrow.png')}
          />
        </View>
      </TouchableOpacity>
      <TouchableOpacity
        style={{
          width: 350,
          height: 50,
          backgroundColor: 'white',
          borderWidth: 1,
          borderColor: 'grey',
          borderRadius: 15,
          marginTop: 25,
          marginHorizontal: 25,
        }}>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'center',
            marginLeft: 20,
          }}>
          <Text
            style={{
              fontSize: 20,
              fontWeight: 'bold',
              // textAlign: 'center',
              marginTop: 10,
            }}>
            React Native
          </Text>
          <Image
            style={{marginTop: 18, marginLeft: 150}}
            source={require('../../assets/images/Arrow.png')}
          />
        </View>
      </TouchableOpacity>
      <TouchableOpacity
        style={{
          width: 350,
          height: 50,
          backgroundColor: 'white',
          borderWidth: 1,
          borderColor: 'grey',
          borderRadius: 15,
          marginTop: 25,
          marginHorizontal: 25,
        }}>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'center',
            marginLeft: 20,
          }}>
          <Text
            style={{
              fontSize: 20,
              fontWeight: 'bold',
              // textAlign: 'center',
              marginTop: 10,
            }}>
            React Native
          </Text>
          <Image
            style={{marginTop: 18, marginLeft: 150}}
            source={require('../../assets/images/Arrow.png')}
          />
        </View>
      </TouchableOpacity>
    </View>
  );
};
export default IntroductScreen;
