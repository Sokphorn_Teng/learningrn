import React from 'react';
import {Text, TouchableOpacity, View, Image, ScrollView} from 'react-native';

const HomeScreen = ({navigation}) => {
  return (
    <ScrollView style={{flex: 1, backgroundColor: '#74ebd5'}}>
      <View style={{flexDirection: 'row'}} showsVerticalScrollIndicator={false}>
        <View
          style={{
            marginHorizontal: 9,
            borderWidth: 1,
            width: 190,
            height: 210,
            marginTop: 50,
            borderRadius: 20,
            borderColor: 'green',
          }}>
          <TouchableOpacity
            onPress={() => {
              navigation.navigate('starting');
            }}
            style={{
              width: 150,
              height: 150,
              marginTop: 10,
              marginHorizontal: 17,
            }}>
            <Image
              source={require('../../assets/images/img1.png')}
              style={{width: 150, height: 150, borderRadius: 20}}
            />
            <Text
              style={{
                fontSize: 20,
                fontWeight: 'bold',
                textAlign: 'center',
                color: 'green',
              }}>
              Starting
            </Text>
          </TouchableOpacity>
        </View>
        <View
          style={{
            marginHorizontal: 4,
            borderWidth: 1,
            width: 190,
            height: 210,
            marginTop: 50,
            borderRadius: 20,
            borderColor: 'green',
          }}>
          <TouchableOpacity
            onPress={() => {
              navigation.navigate('setup');
            }}
            style={{
              width: 150,
              height: 150,
              marginTop: 10,
              marginHorizontal: 17,
            }}>
            <Image
              source={require('../../assets/images/img3.png')}
              style={{width: 150, height: 150, borderRadius: 20}}
            />
            <Text
              style={{
                fontSize: 20,
                fontWeight: 'bold',
                textAlign: 'center',
                color: 'green',
              }}>
              Setup
            </Text>
          </TouchableOpacity>
        </View>
      </View>
      {/* More under img */}
      <View style={{flexDirection: 'row'}} showsVerticalScrollIndicator={false}>
        <View
          style={{
            marginHorizontal: 9,
            borderWidth: 1,
            width: 190,
            height: 210,
            marginTop: 50,
            borderRadius: 20,
            borderColor: 'green',
          }}>
          <TouchableOpacity
            style={{
              width: 150,
              height: 150,
              marginTop: 10,
              marginHorizontal: 17,
            }}>
            <Image
              source={require('../../assets/images/img2.png')}
              style={{width: 150, height: 150, borderRadius: 20}}
            />
            <Text
              style={{
                fontSize: 20,
                fontWeight: 'bold',
                textAlign: 'center',
                color: 'green',
              }}>
              Basic
            </Text>
          </TouchableOpacity>
        </View>
        <View
          style={{
            marginHorizontal: 4,
            borderWidth: 1,
            width: 190,
            height: 210,
            marginTop: 50,
            borderRadius: 20,
            borderColor: 'green',
          }}>
          <TouchableOpacity
            onPress={() => {
              navigation.navigate('component');
            }}
            style={{
              width: 150,
              height: 150,
              marginTop: 10,
              marginHorizontal: 17,
            }}>
            <Image
              source={require('../../assets/images/img4.png')}
              style={{width: 150, height: 150, borderRadius: 20}}
            />
            <Text
              style={{
                fontSize: 20,
                fontWeight: 'bold',
                textAlign: 'center',
                color: 'green',
              }}>
              Components
            </Text>
          </TouchableOpacity>
        </View>
      </View>
      {/* More img */}
      <View style={{flexDirection: 'row'}} showsVerticalScrollIndicator={false}>
        <View
          style={{
            marginHorizontal: 9,
            borderWidth: 1,
            width: 190,
            height: 210,
            marginTop: 50,
            borderRadius: 20,
            borderColor: 'green',
          }}>
          <TouchableOpacity
            style={{
              width: 150,
              height: 150,
              marginTop: 10,
              marginHorizontal: 17,
            }}>
            <Image
              source={require('../../assets/images/img5.jpg')}
              style={{width: 150, height: 150, borderRadius: 20}}
            />
            <Text
              style={{
                fontSize: 20,
                fontWeight: 'bold',
                textAlign: 'center',
                color: 'green',
              }}>
              API
            </Text>
          </TouchableOpacity>
        </View>
        <View
          style={{
            marginHorizontal: 4,
            borderWidth: 1,
            width: 190,
            height: 210,
            marginTop: 50,
            borderRadius: 20,
            borderColor: 'green',
          }}>
          <TouchableOpacity
            style={{
              width: 150,
              height: 150,
              marginTop: 10,
              marginHorizontal: 17,
            }}>
            <Image
              source={require('../../assets/images/img6.png')}
              style={{width: 150, height: 150, borderRadius: 20}}
            />
            <Text
              style={{
                fontSize: 20,
                fontWeight: 'bold',
                textAlign: 'center',
                color: 'green',
              }}>
              Life Cycle
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    </ScrollView>
  );
};
export default HomeScreen;
